<?php

namespace App\Entity;

use App\Repository\TrickRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: TrickRepository::class)]
#[UniqueEntity('name')]
#[ORM\Table(name: 'trick')]
class Trick
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(name:'name', type: 'string', length: 150, unique: true)]
    private string $name;

    #[ORM\Column(name: 'date_create',type: Types::DATETIME_MUTABLE,nullable: false)]
    private DateTimeInterface $date_create;

    #[ORM\Column(name: 'date_update',type: Types::DATETIME_MUTABLE,nullable: true)]
    private ?DateTimeInterface $date_update;

    #[ORM\Column(name: 'description',type: 'string')]
    private string $description;

    #[ORM\OneToMany(mappedBy: 'trick', targetEntity: Upload::class,cascade: ['persist','remove'])]
    private Collection $upload;

    #[ORM\OneToMany(mappedBy: 'trick', targetEntity: Comment::class,cascade: ['remove'])]
    private Collection $comment;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name:'user_id',referencedColumnName: 'id',nullable: false)]
    private UserInterface $user;


    public function __construct()
    {
        $this->upload = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Trick
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Trick
    {
        $this->name = $name;
        return $this;
    }

    public function getDateCreate(): DateTimeInterface
    {
        return $this->date_create;
    }

    public function setDateCreate(DateTimeInterface $date_create): Trick
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function getDateUpdate(): ?DateTimeInterface
    {
        return $this->date_update;
    }

    public function setDateUpdate(?DateTimeInterface $date_update): Trick
    {
        $this->date_update = $date_update;
        return $this;
    }


    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Trick
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<Upload>
     */
    public function getUpload(): Collection
    {
        return $this->upload;
    }

    public function addUpload(Upload $upload): void
    {
        $this->upload->add($upload);

    }

    public function removeUpload(Upload $upload)
    {
        $this->upload->removeElement($upload);
    }

    /**
     * @return Collection<Comment>
     */
    public function getComment(): Collection
    {
        return $this->comment;
    }

    public function addComment(Comment $comment): void
    {
        $this->comment->add($comment);

    }

    public function removeComment(Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): Trick
    {
        $this->user = $user;
        return $this;
    }



}