<?php

namespace App\Entity;

use App\Repository\UploadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[ORM\Entity(repositoryClass: UploadRepository::class)]
#[ORM\Table(name: 'upload')]
class Upload
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 150, nullable: false)]
    private string $file;

    private UploadedFile $uploadFile;

    #[ORM\ManyToOne(targetEntity: Trick::class, inversedBy: 'upload')]
    #[ORM\JoinColumn(name: 'trick_id', referencedColumnName: 'id', nullable: false)]
    protected ?Trick $trick;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Upload
    {
        $this->id = $id;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): Upload
    {
        $this->file = $file;
        return $this;
    }

    public function getUploadFile(): UploadedFile
    {
        return $this->uploadFile;
    }


    public function setUploadFile(UploadedFile $uploadFile): Upload
    {
        $this->uploadFile = $uploadFile;
        return $this;
    }

    public function getTrick(): ?Trick
    {
        return $this->trick;
    }

    public function setTrick(?Trick $trick): Upload
    {
        $this->trick = $trick;
        return $this;
    }




}