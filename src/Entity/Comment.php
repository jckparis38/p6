<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ORM\Table(name: 'comment')]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private string $id;

    #[ORM\Column(type: 'string')]
    private string $comment;

    #[ORM\ManyToOne(targetEntity: Trick::class, inversedBy: 'comment')]
    #[ORM\JoinColumn(name: 'trick_id', referencedColumnName: 'id', nullable: false)]
    protected Trick $trick;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    protected UserInterface $user;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Comment
    {
        $this->id = $id;
        return $this;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function setComment(string $comment): Comment
    {
        $this->comment = $comment;
        return $this;
    }

    public function getTrick(): Trick
    {
        return $this->trick;
    }

    public function setTrick(Trick $trick): Comment
    {
        $this->trick = $trick;
        return $this;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): Comment
    {
        $this->user = $user;
        return $this;
    }




}