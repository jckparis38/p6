<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentFormType;
use App\Form\TrickFormType;
use App\Repository\TrickRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TrickController extends AbstractController
{
    #[Route('/trick', name: 'trick_index')]
    public function index(TrickRepository $trickRepository):Response
    {
        $tricks = $trickRepository->findBy(array(),array('date_create'=>'DESC'));
//        dump($tricks);
        return $this->render('trick/trick.html.twig',['tricks'=>$tricks]);
    }

    #[Route('/trick/detail/{id}', name: 'trick_detail')]
    public function trickdetail(Request $request,TrickRepository $trickRepository,int $id,EntityManagerInterface $entityManager):Response
    {
        $tricks = $trickRepository->findBy(array('id'=>$id));
        $comment = new Comment();
        $form = $this->createForm(CommentFormType::class,$comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($this->getUser());
            $comment->setTrick($tricks[0]);
            $entityManager->persist($comment);
            $entityManager->flush();
            unset($comment);
        }
        return $this->render('trick/trickdetail.html.twig',['trick'=>$tricks[0],'commentForm'=>$form->createView()]);
    }

    #[Route('/savetrick', name: 'trick_save')]
    public function addTrick(Request $request,EntityManagerInterface $entityManager):Response
    {
        $trick = new Trick();
        $form = $this->createForm(TrickFormType::class, $trick);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $trick->setUser($this->getUser());
            $trick->setDateCreate( new \DateTime());
            $entityManager->persist($trick);
            dump(count($trick->getUpload()));
            if(count($trick->getUpload())>0) {
                foreach ($trick->getUpload() as $upload) {
                    $path = sha1(uniqid(mt_rand(), true)) . '.' . $upload->getUploadFile()->guessExtension();
                    $upload->getUploadFile()->move('/var/www/html/public/img/upload/', $path);
                    $upload->setFile($path);
                    $upload->setTrick($trick);

                    $entityManager->persist($upload);
                }
                $entityManager->flush();
                unset($trick);
                unset($form);
                $trick = new Trick();
                $form = $this->createForm(TrickFormType::class, $trick);
                $this->addFlash('success', "Add success !");
            }
            else{
                $this->addFlash('error', "An image is required !");
            }
            $this->redirectToRoute('trick_save');
        }
        return $this->render('trick/tricksave.html.twig', [
            'trickForm' => $form->createView(),
        ]);
    }

    #[Route('/edittrick/{id}', name: 'trick_edit')]
    public function editTrick(Request $request,int $id,EntityManagerInterface $entityManager):Response
    {
        $trick = $entityManager->getRepository(Trick::class)->findBy(['id'=>$id])[0];
        $form = $this->createForm(TrickFormType::class, $trick);
//        dump($trick->getUpload());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $trick->setUser($this->getUser());
            $trick->setDateUpdate( new \DateTime());
            $entityManager->persist($trick);

            foreach ($trick->getUpload() as $upload)
            {
                $path = sha1(uniqid(mt_rand(), true)).'.'.$upload->getUploadFile()->guessExtension();
                $upload->getUploadFile()->move('/var/www/html/public/img/upload/', $path);
                $upload->setFile($path);
                $upload->setTrick($trick);

                $entityManager->persist($upload);
            }
            $entityManager->flush();
            $this->addFlash('success', "Add success !");
        }
        return $this->render('trick/trickedit.html.twig', [
            'trickForm' => $form->createView(),
        ]);
    }

    #[Route('/removetrick', name: 'trick_remove')]
    public function removeTrick(Request $request,TrickRepository $trickRepository): JsonResponse
    {
        $id = $request->query->get('id');
        $trick = $trickRepository->findBy(array('id'=>$id))[0];
        $trickRepository->remove($trick);
        $verif = $trickRepository->findBy(array('id'=>$id));
        if(empty($verif))
        {
            return new JsonResponse(['message' => 'Remove Succes'],Response::HTTP_OK);
        }
        else
        {
            return new JsonResponse(['message' => 'Remove Error'],Response::HTTP_FOUND);
        }
    }
}