<?php
namespace App\Controller;

use App\Repository\TrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'default_index', methods: 'GET')]
    public function index(TrickRepository $trickRepository):Response
    {
        $tricks = $trickRepository->findBy(array(),array('date_create'=>'DESC'));
//        dump($tricks[0]->getUpload()->toArray());
        return $this->render('index/index.html.twig',['tricks'=>$tricks]);
    }
}