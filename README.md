P6 - Le site communautaire SnowTricks

<h3>Diagrammes UML</h3>
<ul>
  <li>Diagramme de cas d'utilsation</li>
  <li>Diagramme de séquence</li>
  <li>MPD</li>
</ul>

<h3>Langage de programmation</h3>

<ul>
</ul>
<li>Le site communautaire SnowTricks a été développé en PHP via le framework Symfony 6.2</li>

<hr>
<h2>Installation</h2>
<h3>Environnement nécessaire</h3>
<ul>
  <li>Symfony 6.2.*</li>
  <li>PHP 8.2</li>
  <li>MySql 8</li>
</ul>
<h3>Suivre les étapes suivantes :</h3>
<ul>
  <li><b>Etape 1 :</b> Cloner le repository suivant depuis votre terminal :</li>
  <pre>
  <code>git clone https://gitlab.com/jckparis38/p6.git</code></pre>
   <li><b>Etape 2 :</b> Executer les commandes suivante :</li>
<pre>
  <code>Allez dans le dossier :  cd p6</code>
</pre>
    <pre>
  <code>Install + Execute le composant : docker-compose up -d</code>
</pre>
<pre>
  <code>Donner les droits d'accés : sudo chown -R www-data:www-data public/img</code>
</pre>
    <pre>
  <code>Entrer dans le docker : docker compose exec app bash</code></pre>  
    <pre>
  <code>composer install</code>
</pre>
    <pre>
  <code>yarn install</code>
</pre>
<pre>
  <code>Build le CSS & JS : yarn encore dev</code>
</pre>
</ul>

<h3>Vous êtes fin prêt pour tester l'application </h3>
<p>Pour afficher le site en ligne rendez-vous à l'adresse suivante votre navigateur : <em>http://127.0.0.1</em></p>