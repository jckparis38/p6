INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (26, 'Les grabs', '2023-02-02 17:40:14', null, 'n grab consiste à attraper la planche avec la main pendant le saut. Le verbe anglais to grab signifie « attraper. »

Il existe plusieurs types de grabs selon la position de la saisie et la main choisie pour l\'effectuer, avec des difficultés variables :

mute : saisie de la carre frontside de la planche entre les deux pieds avec la main avant ;
sad ou melancholie ou style week : saisie de la carre backside de la planche, entre les deux pieds, avec la main avant ;
indy : saisie de la carre frontside de la planche, entre les deux pieds, avec la main arrière ;', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (27, 'Les rotations', '2023-02-02 17:42:44', null, 'On désigne par le mot « rotation » uniquement des rotations horizontales ; les rotations verticales sont des flips. Le principe est d\'effectuer une rotation horizontale pendant le saut, puis d\'attérir en position switch ou normal. La nomenclature se base sur le nombre de degrés de rotation effectués  :

un 180 désigne un demi-tour, soit 180 degrés d\'angle ;
360, trois six pour un tour complet ;
540, cinq quatre pour un tour et demi ;
720, sept deux pour deux tours complets ;
900 pour deux tours et demi ;
1080 ou big foot pour trois tours ;
etc.', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (28, 'Les flips', '2023-02-02 17:45:05', null, 'Un flip est une rotation verticale. On distingue les front flips, rotations en avant, et les back flips, rotations en arrière.

Il est possible de faire plusieurs flips à la suite, et d\'ajouter un grab à la rotation.

Les flips agrémentés d\'une vrille existent aussi (Mac Twist, Hakon Flip...), mais de manière beaucoup plus rare, et se confondent souvent avec certaines rotations horizontales désaxées.

Néanmoins, en dépit de la difficulté technique relative d\'une telle figure, le danger de retomber sur la tête ou la nuque est réel et conduit certaines stations de ski à interdire de telles figures dans ses snowparks.', 7);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (29, 'Les rotations désaxées', '2023-02-02 17:47:49', null, 'Une rotation désaxée est une rotation initialement horizontale mais lancée avec un mouvement des épaules particulier qui désaxe la rotation. Il existe différents types de rotations désaxées (corkscrew ou cork, rodeo, misty, etc.) en fonction de la manière dont est lancé le buste. Certaines de ces rotations, bien qu\'initialement horizontales, font passer la tête en bas.

Bien que certaines de ces rotations soient plus faciles à faire sur un certain nombre de tours (ou de demi-tours) que d\'autres, il est en théorie possible de d\'attérir n\'importe quelle rotation désaxée avec n\'importe quel nombre de tours, en jouant sur la quantité de désaxage afin de se retrouver à la position verticale au moment voulu.

Il est également possible d\'agrémenter une rotation désaxée par un grab', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (30, 'Les slides', '2023-02-02 17:48:59', null, 'Un slide consiste à glisser sur une barre de slide. Le slide se fait soit avec la planche dans l\'axe de la barre, soit perpendiculaire, soit plus ou moins désaxé.

On peut slider avec la planche centrée par rapport à la barre (celle-ci se situe approximativement au-dessous des pieds du rideur), mais aussi en nose slide, c\'est-à-dire l\'avant de la planche sur la barre, ou en tail slide, l\'arrière de la planche sur la barre.', 7);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (31, 'Les one foot tricks', '2023-02-02 17:50:37', null, 'Figures réalisée avec un pied décroché de la fixation, afin de tendre la jambe correspondante pour mettre en évidence le fait que le pied n\'est pas fixé. Ce type de figure est extrêmement dangereuse pour les ligaments du genou en cas de mauvaise réception.', 7);
