create table doctrine_migration_versions
(
    version        varchar(191) not null
        primary key,
    executed_at    datetime     null,
    execution_time int          null
)
    collate = utf8mb3_unicode_ci;

create table role
(
    id           int auto_increment
        primary key,
    libelle_role varchar(50) not null
)
    collate = utf8mb4_unicode_ci;

create table user
(
    id          int auto_increment
        primary key,
    email       varchar(180) not null,
    role_id     int          not null,
    password    varchar(255) not null,
    nom         varchar(50)  not null,
    prenom      varchar(50)  not null,
    username    varchar(50)  not null,
    is_verified tinyint(1)   not null,
    constraint UNIQ_8D93D649F85E0677
        unique (username),
    constraint FK_8D93D649D60322AC
        foreign key (role_id) references role (id)
)
    collate = utf8mb4_unicode_ci;

create table reset_password_request
(
    id           int auto_increment
        primary key,
    user_id      int          not null,
    selector     varchar(20)  not null,
    hashed_token varchar(100) not null,
    requested_at datetime     not null comment '(DC2Type:datetime_immutable)',
    expires_at   datetime     not null comment '(DC2Type:datetime_immutable)',
    constraint FK_7CE748AA76ED395
        foreign key (user_id) references user (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_7CE748AA76ED395
    on reset_password_request (user_id);

create table trick
(
    id          int auto_increment
        primary key,
    name        varchar(150) not null,
    date_create datetime     not null,
    date_update datetime     null,
    description text         null,
    user_id     int          not null,
    constraint UNIQ_D8F0A91E5E237E06
        unique (name),
    constraint FK_D8F0A91EA76ED395
        foreign key (user_id) references user (id)
);

create table comment
(
    id       int auto_increment
        primary key,
    comment  varchar(255) not null,
    trick_id int          not null,
    user_id  int          not null,
    constraint comment_trick_id_fk
        foreign key (trick_id) references trick (id),
    constraint comment_user_id_fk
        foreign key (user_id) references user (id)
);

create index IDX_9474526CA76ED395
    on comment (user_id);

create index IDX_9474526CB281BE2E
    on comment (trick_id);

create index IDX_D8F0A91EA76ED395
    on trick (user_id);

create table upload
(
    id       int auto_increment
        primary key,
    file     varchar(150) not null,
    trick_id int          not null,
    constraint upload_trick_id_fk
        foreign key (trick_id) references trick (id)
);

create index IDX_17BDE61FB281BE2E
    on upload (trick_id);

create index IDX_8D93D649D60322AC
    on user (role_id);


INSERT INTO p6.role (id, libelle_role) VALUES (1, 'ROLE_ADMIN');
INSERT INTO p6.role (id, libelle_role) VALUES (2, 'ROLE_USER');

INSERT INTO p6.user (id, email, role_id, password, nom, prenom, username, is_verified) VALUES (1, 'parisjc@gmail.com', 1, '$2y$13$Wz28FAv6ARMv2xy8uaISf.lGHb347XrMrOfBMvidR3up9ULlBRX4W', 'PARIS', 'Jean-Christophe', 'parisjc', 0);
INSERT INTO p6.user (id, email, role_id, password, nom, prenom, username, is_verified) VALUES (7, 'jckparis38@gmail.com', 2, '$2y$13$cDjNp1yyGHnOe7yRwXTavuiULDmibHi/MnNOSe1XcRVEFPGjhTSNS', 'User', 'User', 'user', 1);

INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (26, 'Les grabs', '2023-02-02 17:40:14', null, 'n grab consiste à attraper la planche avec la main pendant le saut. Le verbe anglais to grab signifie « attraper. »

Il existe plusieurs types de grabs selon la position de la saisie et la main choisie pour l\'effectuer, avec des difficultés variables :

mute : saisie de la carre frontside de la planche entre les deux pieds avec la main avant ;
sad ou melancholie ou style week : saisie de la carre backside de la planche, entre les deux pieds, avec la main avant ;
indy : saisie de la carre frontside de la planche, entre les deux pieds, avec la main arrière ;', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (27, 'Les rotations', '2023-02-02 17:42:44', null, 'On désigne par le mot « rotation » uniquement des rotations horizontales ; les rotations verticales sont des flips. Le principe est d\'effectuer une rotation horizontale pendant le saut, puis d\'attérir en position switch ou normal. La nomenclature se base sur le nombre de degrés de rotation effectués  :

un 180 désigne un demi-tour, soit 180 degrés d\'angle ;
360, trois six pour un tour complet ;
540, cinq quatre pour un tour et demi ;
720, sept deux pour deux tours complets ;
900 pour deux tours et demi ;
1080 ou big foot pour trois tours ;
etc.', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (28, 'Les flips', '2023-02-02 17:45:05', null, 'Un flip est une rotation verticale. On distingue les front flips, rotations en avant, et les back flips, rotations en arrière.

Il est possible de faire plusieurs flips à la suite, et d\'ajouter un grab à la rotation.

Les flips agrémentés d\'une vrille existent aussi (Mac Twist, Hakon Flip...), mais de manière beaucoup plus rare, et se confondent souvent avec certaines rotations horizontales désaxées.

Néanmoins, en dépit de la difficulté technique relative d\'une telle figure, le danger de retomber sur la tête ou la nuque est réel et conduit certaines stations de ski à interdire de telles figures dans ses snowparks.', 7);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (29, 'Les rotations désaxées', '2023-02-02 17:47:49', null, 'Une rotation désaxée est une rotation initialement horizontale mais lancée avec un mouvement des épaules particulier qui désaxe la rotation. Il existe différents types de rotations désaxées (corkscrew ou cork, rodeo, misty, etc.) en fonction de la manière dont est lancé le buste. Certaines de ces rotations, bien qu\'initialement horizontales, font passer la tête en bas.

Bien que certaines de ces rotations soient plus faciles à faire sur un certain nombre de tours (ou de demi-tours) que d\'autres, il est en théorie possible de d\'attérir n\'importe quelle rotation désaxée avec n\'importe quel nombre de tours, en jouant sur la quantité de désaxage afin de se retrouver à la position verticale au moment voulu.

Il est également possible d\'agrémenter une rotation désaxée par un grab', 1);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (30, 'Les slides', '2023-02-02 17:48:59', null, 'Un slide consiste à glisser sur une barre de slide. Le slide se fait soit avec la planche dans l\'axe de la barre, soit perpendiculaire, soit plus ou moins désaxé.

On peut slider avec la planche centrée par rapport à la barre (celle-ci se situe approximativement au-dessous des pieds du rideur), mais aussi en nose slide, c\'est-à-dire l\'avant de la planche sur la barre, ou en tail slide, l\'arrière de la planche sur la barre.', 7);
INSERT INTO p6.trick (id, name, date_create, date_update, description, user_id) VALUES (31, 'Les one foot tricks', '2023-02-02 17:50:37', null, 'Figures réalisée avec un pied décroché de la fixation, afin de tendre la jambe correspondante pour mettre en évidence le fait que le pied n\'est pas fixé. Ce type de figure est extrêmement dangereuse pour les ligaments du genou en cas de mauvaise réception.', 7);

INSERT INTO p6.upload (id, file, trick_id) VALUES (21, '6bc33b9add772a8336ea924096c3843620a7af30.jpg', 26);
INSERT INTO p6.upload (id, file, trick_id) VALUES (22, '68b4daad5ad9b74593f312ef340fcc49cf413d2f.jpg', 27);
INSERT INTO p6.upload (id, file, trick_id) VALUES (23, '6d0c2d10f34eab3779849d2d1c37aee3fd5ea6b7.jpg', 27);
INSERT INTO p6.upload (id, file, trick_id) VALUES (24, 'e94e0f338b3d0a0ce668a1a4ba83b7d6b847ec30.jpg', 28);
INSERT INTO p6.upload (id, file, trick_id) VALUES (25, '04b33f6f64acdec9b0195404bc83d74bbe563772.jpg', 28);
INSERT INTO p6.upload (id, file, trick_id) VALUES (26, 'c525d1b743651cb085ba58205a2c048d7c9fe43f.jpg', 29);
INSERT INTO p6.upload (id, file, trick_id) VALUES (27, '44464e348e52938fe1e1a240784c59e6bae44eaa.jpg', 29);
INSERT INTO p6.upload (id, file, trick_id) VALUES (28, 'a04cf813254aecc623cf5150aae9f9991fb7da70.jpg', 30);
INSERT INTO p6.upload (id, file, trick_id) VALUES (29, 'a326ee4d48d0916bfec2414de8e3b7921e24756c.jpg', 30);
INSERT INTO p6.upload (id, file, trick_id) VALUES (30, 'd9bb314e619cc3ae306a6eba81a891ecf451e8c5.jpg', 31);
INSERT INTO p6.upload (id, file, trick_id) VALUES (31, '19846f399c880ef578db3e1ea8898da6664ff4e4.jpg', 31);
