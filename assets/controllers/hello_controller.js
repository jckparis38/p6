import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static values = {
        idremove: Number,
        idedit: Number
    }

    removetrick() {
        if (confirm("About wanting to delete!") === true) {
            fetch('/removetrick?' + new URLSearchParams({ id : this.idremoveValue}), {'method': 'GET', headers: {'X-Requested-With': 'XMLHttpRequest'}})
                .then(res => res.json())
                .then(() => {
                    location.reload();
                })
        }
    }

    edittrick() {
        window.location.href = "/edittrick/" + this.ideditValue
    }
}
